from base64 import decode
from operator import mod
import onnxruntime
import numpy as np
import librosa
from itertools import groupby
import glob
import shutil
import os
import pandas as pd
# from ctcdecode import CTCBeamDecoder
import ds_ctcdecoder as ds
import time

import soundfile as sf
import torch
from get_speech_features import get_speech_features

labels=[] #, "а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л","м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", "|"]
# vocab=["а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л","м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", " "]


def get_list_files(path, ignore_path=None, contain=None, endwith=None):
    '''
    :param path: path to files :)
    :param ignore_path: name or part of name path, which not include for parsing
    :return: numpy ndarray with list of full path files
    '''
    list_files = []
    for root, dirs, files in os.walk(path, followlinks=True):
        for file in files:
            file_path = os.path.join(root, file)
            if ignore_path is not None:
                if np.any(list(map(lambda x: x in file_path, ignore_path))):
                    continue
            if contain is not None:
                if np.any(list(map(lambda x: x in file_path, contain))):
                    list_files.append(file_path)
                else:
                    continue
            if endwith is not None:
                if file_path.endswith(endwith):
                    list_files.append(file_path)
                else:
                    continue
            else:
                list_files.append(file_path)

    return list_files

# для самопроверки вчитаем алфавит
f = open('lm/alphabet.txt', 'r')
for line in f.readlines():
    ll = line[:-1]
    print(ll)
    labels.append(ll)
labels.append('.')

# # labels.append('|')
# print(len(labels))

_sample_rate = 16000
window_size=0.025
window_stride=0.01

if window_size:
    n_window_size = int(window_size * _sample_rate)
if window_stride:
    n_window_stride = int(window_stride * _sample_rate)


sess = onnxruntime.InferenceSession("en-z.onnx", providers=['CPUExecutionProvider'])

model_path = "lm/denti.scorer"

abet = ds.Alphabet('lm/alphabet.txt')
scorer = ds.Scorer(0.0, 0.0, model_path, abet)


print('Loaded')

a = get_list_files('files')

for f in sorted(a):
    print(f)
    startTime = time.time()
    signal, fs = sf.read(f, dtype=np.float32)

    feat = get_speech_features(signal, fs, num_features=64,
                                        features_type='mel',
                                        n_fft=512,
                                        win_length=n_window_size,
                                        hop_length=n_window_stride,
                                        mag_power=2,
                                        feature_normalize=True)

    input_name = sess.get_inputs()[0].name
    output_name = sess.get_outputs()[0].name

    # encoder
    result = sess.run( [output_name], { input_name: feat.T.reshape(1, 64, -1) } )


    data = np.exp(result[0][0].astype(np.float64))
    #np.save("z.npy", data)
    #print(data.shape)
    #print(data[0:4,0:4])

    # log_probs_length = [ feat.shape[0] ]
    # log_probs = torch.from_numpy(result[0])

    beam_results = ds.ctc_beam_search_decoder(data, abet, 1000, 0.97, 10, scorer)
    print(beam_results[0][1])
    # beam_results, beam_scores, timesteps, out_lens = decoder.decode(log_probs)
    # print('Decoded')
    # len = out_lens[0][0]
    # hypo = beam_results[0][0][:len ]

    d = result[0].argmax(axis=2)[0]
    # print(d)
    L = ''.join([labels[x] for x in result[0].argmax(axis=2)[0]])
    #print(L)
    asdf = ''.join([x[0] for x in groupby(L)]).replace('.', '').replace('  ', ' ')

    executionTime = (time.time() - startTime)
    print(asdf)
    # H = ''.join([labels[x] for x in hypo])
    # print(H)
    print('Execution time in seconds: ' + str(executionTime))
