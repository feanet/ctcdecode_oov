Project DeepSpeech
==================

DeepSpeech is an open-source Speech-To-Text engine, using a model trained by machine learning techniques based on `Baidu's Deep Speech research paper <https://arxiv.org/abs/1412.5567>`_. 

This is my experiment on having very small LM - consisting of a few specific terms.
ctcdecode recognizes well these words, spell-check raw output to correct word, however
completely ignores non-vocabulary words. I modified path_score.cc to continue adding
new_char when *matcher* stopped finding the expanded word.

So beams would now contain sentences with OOV and with correct words. I sort first correct words,
and now it depends on comparison function, which beams will stay - correct first of all.
