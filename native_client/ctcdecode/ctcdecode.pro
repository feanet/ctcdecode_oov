TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ../alphabet.cc \
        cnpy.cpp \
        ctc_beam_search_decoder.cpp \
        decoder_utils.cpp \
        experiment.cpp \
        path_trie.cpp \
        scorer.cpp

DEFINES += KENLM_MAX_ORDER=3
INCLUDEPATH += /home/alex/src/kaldi/tools/openfst-1.7.2/include
INCLUDEPATH += /home/alex/src/DeepSpeech/native_client
INCLUDEPATH += /home/alex/src/DeepSpeech/native_client/ctcdecode/third_party/object_pool
INCLUDEPATH += /home/alex/src/DeepSpeech/native_client/ctcdecode/third_party/ThreadPool
INCLUDEPATH += /home/alex/src/DeepSpeech/native_client/kenlm
#LIBS += -L/home/alex/src/DeepSpeech/native_client/kenlm/build/lib -l:libkenlm.a -l:libkenlm_util.a
#LIBS += -L/home/alex/src/kaldi/tools/openfst-1.7.2/lib -l:libfst.a

LIBS += -L/home/alex/src/DeepSpeech/native_client/ctcdecode/libs/ -l:libkenlm.a -l:libkenlm_util.a -l:libfst.a
LIBS += -lz -llzma -lbz2 -pthread

HEADERS += \
    cnpy.h \
    ctc_beam_search_decoder.h \
    path_trie.h \
    scorer.h
