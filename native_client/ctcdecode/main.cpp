#include "cnpy.h"
#include "ctc_beam_search_decoder.h"
#include <iostream>

using namespace std;

int main()
{
   cnpy::NpyArray asdf = cnpy::npy_load("/media/alex/NNET51/16k/Models/z.npy");
    Alphabet a;
    a.init("/media/alex/NNET51/16k/Models/alphabet.txt");

    std::shared_ptr<Scorer> s(new Scorer);
    s->init("/media/alex/NNET51/16k/Models/denti.scorer", a);

    std::unordered_map<std::string, float> hw;

    std::vector<Output> out =
    ctc_beam_search_decoder(asdf.data<double>(), asdf.shape[0], asdf.shape[1], a, 1000, 0.98, 20, s, hw, 30 );


    for (int i = 0; i < out.size(); ++i)
        cout << a.Decode(out[i].tokens) << endl;



    return 0;
}
